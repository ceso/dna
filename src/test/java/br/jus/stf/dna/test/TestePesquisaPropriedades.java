package br.jus.stf.dna.test;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import br.jus.stf.dna.properties.DatabaseProperties;
import br.jus.stf.dna.repository.DnaRepository;

public class TestePesquisaPropriedades extends AbstractTeste {

	@Autowired
	private DnaRepository dnaRepository;

	@Test
	public void getPropriedades() {
		ArrayList<String> sistemas = new ArrayList<String>();
		sistemas.add("EPET");
		sistemas.add("PROCINICIAL");
		DatabaseProperties databaseProperties = new DatabaseProperties(dnaRepository, sistemas);
		String carimbadoras = (String) databaseProperties.getProperty("seguranca.icp.carimbadoras");
		Assert.assertNotNull(carimbadoras);
	}
	
}
