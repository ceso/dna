package br.jus.stf.dna.test.configuration;

import javax.sql.DataSource;

import oracle.jdbc.OracleDriver;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;

import br.jus.stf.dna.configuration.DnaContextConfiguration;
import br.jus.stf.seguranca.datasource.LocalDataSourceSTF;
import br.jus.stf.seguranca.datasource.PackageSegurancaEnum;

@Configuration
@Profile("teste")
public class ContextConfigurationTeste extends DnaContextConfiguration {

	@Bean
	public DataSource dataSource() {
			
		SimpleDriverDataSource simpleDriverDataSource = new SimpleDriverDataSource();
		simpleDriverDataSource.setDriverClass(OracleDriver.class);
		simpleDriverDataSource.setUrl("jdbc:oracle:thin:@bd-orcl-des:1521:stfd");
		simpleDriverDataSource.setUsername("USUARIOWSEPET");
		simpleDriverDataSource.setPassword("d%usuariowsepet08");
		
		LocalDataSourceSTF dataSource = new LocalDataSourceSTF();
		dataSource.setDataSource(simpleDriverDataSource);
		dataSource.setSistema("EPET");
		dataSource.setPackageSeguranca(PackageSegurancaEnum.PRC_SEGURANCA_JBOSS);
		dataSource.setSomenteUsuarioFixo(true);
		dataSource.setUsuarioAnonimo("LUCAS.RODRIGUES@STF.JUS.BR");
		return dataSource;
	}
	
}
