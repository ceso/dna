package br.jus.stf.dna.test;

import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.jus.stf.dna.test.configuration.ContextConfigurationTeste;

@ActiveProfiles("teste")
@ContextConfiguration(classes={ContextConfigurationTeste.class})
@RunWith(SpringJUnit4ClassRunner.class)
public abstract class AbstractTeste {

}
