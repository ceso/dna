package br.jus.stf.dna.repository;

import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

/**
 * Repositório de configurações
 * 
 * @author Lucas.Rodrigues
 *
 */
@Repository("dnaRepository")
public class DnaRepository {
	
	private static final String TABLE = "GLOBAL.CONFIGURACAO_SISTEMA";
	private static final String NAME_COLUMN = "SIG_SISTEMA";
	private static final String KEY_COLUMN = "DSC_CHAVE_CONFIGURACAO";
	private static final String VALUE_COLUMN = "TXT_VALOR_CONFIGURACAO";
	
	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}
		
	/**
	 * Pesquisa as propriedades de um sistema. 
	 * 
	 * @param sistema
	 * @return mapa com as propriedades carregadas.
	 */
	public Map<String, Object> findSistemaProperties(List<String> sistemas) {
		
		StringBuilder query = new StringBuilder("SELECT DISTINCT " + KEY_COLUMN + "," + VALUE_COLUMN + " FROM " + TABLE);
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		if (sistemas != null && sistemas.size() > 0) {		
			query.append(" WHERE " + NAME_COLUMN + " IN (:sistemas) ");
			query.append("OR " + NAME_COLUMN + " is null");
			parameters.addValue("sistemas", sistemas, Types.VARCHAR);
		} else {
			query.append(" WHERE " + NAME_COLUMN + " is null");
		}
		
		Assert.notNull(namedParameterJdbcTemplate, "O namedParameterJdbcTemplate não pode ser nulo.");
		
		return getResult(namedParameterJdbcTemplate.queryForList(query.toString(), parameters));
	}
	
	private Map<String, Object> getResult(List<Map<String, Object>> list) {
		
		Map<String, Object> result = new HashMap<String, Object>();
		
		for (Map<String, Object> property : list) {
			result.put((String) property.get(KEY_COLUMN), property.get(VALUE_COLUMN));
		}
		return result;		
	}
	
}
