package br.jus.stf.dna.properties;

import java.util.List;
import java.util.Map;

import org.springframework.core.env.PropertySource;
import org.springframework.dao.DataAccessResourceFailureException;

import br.jus.stf.dna.repository.DnaRepository;


/**
 * @author Lucas.Rodrigues
 *
 */
public class DatabaseProperties extends PropertySource<Map<String, Object>> {
	
	private Map<String, Object> properties;
	
	/**
	 * PropertySource com busca de propriedades na tabela global.configuracao_sistema
	 * 
	 * @param sistema
	 */
	public DatabaseProperties(DnaRepository dnaRepository, List<String> sistemas) {
		
		super("sistemaDatabaseProperties");
		
		if (dnaRepository == null) {
			throw new DataAccessResourceFailureException("O repositório do Dna não pode ser nulo!");
		}
		this.properties = dnaRepository.findSistemaProperties(sistemas);
	}

	/* (non-Javadoc)
	 * @see org.springframework.core.env.PropertySource#getProperty(java.lang.String)
	 */
	@Override
	public Object getProperty(String name) {
		
		if (properties.containsKey(name)) {
			return properties.get(name);
		}
		return null;
	}
	
}
