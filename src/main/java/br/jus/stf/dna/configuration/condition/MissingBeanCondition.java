package br.jus.stf.dna.configuration.condition;

import java.util.Map;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;


/**
 * Verifica se os beans existem no contexto
 * 
 * @author Lucas.Rodrigues
 *
 */
public class MissingBeanCondition implements Condition {

	/* (non-Javadoc)
	 * @see org.springframework.context.annotation.Condition#matches(org.springframework.context.annotation.ConditionContext, org.springframework.core.type.AnnotatedTypeMetadata)
	 */
	@Override
	public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
		
		Map<String, Object> attr = metadata.getAnnotationAttributes("br.jus.stf.dna.configuration.condition.MissingBeanConditional");
		
		if (attr != null && attr.containsKey("name")) {
			
			String[] beanArray = (String[]) attr.get("name");
			int numBeanExists = 0;
			
			for (String beanName : beanArray) {
				if (context.getBeanFactory().containsBean(beanName)) {
					numBeanExists++;
				}
			}
			return numBeanExists == 0;
		}
		return false;
	}

}
