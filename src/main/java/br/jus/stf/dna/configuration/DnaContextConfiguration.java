package br.jus.stf.dna.configuration;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import br.jus.stf.dna.configuration.condition.MissingBeanConditional;

/**
 * Configuração do contexto do dna
 * 
 * @author Lucas.Rodrigues
 *
 */
@Configuration
@ComponentScan("br.jus.stf.dna")
public class DnaContextConfiguration {
	
	/**
	 * Bean do jdbcTemplate que será criado apenas se não existir um outro no contexto.
	 * 
	 * @param dataSource
	 * @return
	 */
	@Bean
	@MissingBeanConditional(name={"namedParameterJdbcTemplate"})
	@DependsOn("dataSource")
	public NamedParameterJdbcTemplate namedParameterJdbcTemplate(DataSource dataSource) {
		return new NamedParameterJdbcTemplate(dataSource);
	}
	
}
