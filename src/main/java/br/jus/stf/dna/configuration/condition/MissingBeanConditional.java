package br.jus.stf.dna.configuration.condition;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.context.annotation.Conditional;

/**
 * Annotation para criar o bean apenas se os beans informados não existirem no contexto.
 * 
 * @author Lucas.Rodrigues
 *
 */
@Target({ ElementType.TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Conditional(MissingBeanCondition.class)
public @interface MissingBeanConditional {

	String[] name() default {};

}

